#!/bin/bash
TARGET_DIR=~/Git/DOC/doc_ui

copyFiles() {
    echo --------- && echo  Project Directory: $TARGET_DIR && echo ---------
    for fileOrDir in '.installer' 'composer.json' 'docker-compose.yml' 'Dockerfile' 'laptop_docker_dev_up.sh';
    do
        echo \* Copying $fileOrDir
        cp -a $fileOrDir $TARGET_DIR
    done
}

if [ -d $TARGET_DIR ];
    then copyFiles
    else
    echo "Target Directory does not exist. Modify TARGET_DIR variable."
    echo "Current path: $TARGET_DIR";
fi


exit $?
