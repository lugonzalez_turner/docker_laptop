# docker_laptop

This project will let you run the current working directory as `/var/www` in a Docker container.

Currently supports:

  - Nginx on Centos 7

#### Prerequisites
  - [Docker Toolbox](https://www.docker.com/products/docker-toolbox) (OS X)
  - Not tested on Linux or Windows, so let me know.


#### Usage

Copy the files to the root of the directory you want as your public_html folder, then run `laptop_docker_dev_up.sh`

One way to do this:

`
cd nginx_laptop
cp -a . ~/Git/doc_ui

`
