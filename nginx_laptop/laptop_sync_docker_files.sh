#!/bin/bash
TARGET_DIR=~/git/doc_ui

copyFiles() {
    echo --------- && echo  Project Directory: $TARGET_DIR && echo ---------
    for fileOrDir in '.installer' 'composer.json' 'docker-compose.yml' 'Dockerfile' 'laptop_docker_dev_up.sh';
    do
        echo \* Copying $fileOrDir
        cp -a $fileOrDir $TARGET_DIR
    done
}

if [ -d $TARGET_DIR ];
    then copyFiles
    else
        echo "Current path (INCORRECT): $TARGET_DIR";
        echo "Modify \$TARGET_DIR variable!"
fi


exit $?
